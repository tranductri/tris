package com.hendisantika.springboot.swagger.repository;

import com.hendisantika.springboot.swagger.model.Football;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FootballRepsitory extends JpaRepository<Football,Long> {
}
