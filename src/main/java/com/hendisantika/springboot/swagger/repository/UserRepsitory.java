package com.hendisantika.springboot.swagger.repository;


import com.hendisantika.springboot.swagger.entities.Book;
import com.hendisantika.springboot.swagger.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepsitory extends JpaRepository<User,Long> {
}
