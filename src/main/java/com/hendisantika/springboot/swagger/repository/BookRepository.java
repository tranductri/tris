package com.hendisantika.springboot.swagger.repository;


import com.hendisantika.springboot.swagger.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {
}
