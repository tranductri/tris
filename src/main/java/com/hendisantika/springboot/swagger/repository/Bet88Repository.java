package com.hendisantika.springboot.swagger.repository;

import com.hendisantika.springboot.swagger.model.Betting;
import com.hendisantika.springboot.swagger.model.Football;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Bet88Repository extends JpaRepository<Betting,Long> {
}
