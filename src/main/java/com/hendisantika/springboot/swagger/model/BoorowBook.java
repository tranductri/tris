package com.hendisantika.springboot.swagger.model;

import javax.persistence.*;

@Entity
public class BoorowBook {
    @Id
    @GeneratedValue
    private Long id;


    @ManyToOne
    @JoinColumn(name = "book",nullable = false)
    Book book;
}
