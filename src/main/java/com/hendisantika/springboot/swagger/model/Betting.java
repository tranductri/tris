package com.hendisantika.springboot.swagger.model;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@Data
@Getter
@Setter
public class Betting {
    public Betting() {
    }

    @OneToMany(mappedBy="betting")
    private Set<Football> footballs;

    @Id
    @GeneratedValue
    Long id;
    String betTeam;
    Integer money;
}
