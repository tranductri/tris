package com.hendisantika.springboot.swagger.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Getter
@Setter
public class Football {
    public Football() {
    }


    @OneToMany(mappedBy="football")
    private Set<Betting> bettings;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "football_betti",
            joinColumns = { @JoinColumn(name = "football") },
            inverseJoinColumns = {@JoinColumn(name = "betting") })
    private Set<Football> footballs = new HashSet<>();


    @Id
    @GeneratedValue

    private Long id;
    private String score;
    private String teamName;
    private String teamWin;
    private String teamlose;

}
