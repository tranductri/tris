package com.hendisantika.springboot.swagger;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException, UnirestException {
        HttpResponse<String> response = Unirest.get("https://football-prediction-api.p.rapidapi.com/api/v2/performance-stats?market=classic")
                .header("x-rapidapi-key", "f0e874d402msh514dcf262e3b9b2p1ed8cbjsn5f11b985bab4")
                .header("x-rapidapi-host", "football-prediction-api.p.rapidapi.com")
                .asString();
        System.out.println(response.getBody());
    }
}
