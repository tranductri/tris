package com.hendisantika.springboot.swagger.controller;


import com.hendisantika.springboot.swagger.entities.Book;
import com.hendisantika.springboot.swagger.model.User;
import com.hendisantika.springboot.swagger.repository.BookRepository;
import com.hendisantika.springboot.swagger.repository.UserRepsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {


    @Autowired
    private BookRepository bookRepository;
    private UserRepsitory userRepsitory;

    @GetMapping("/giaithua")
    public Integer factorial(@RequestParam("n") Integer n){
        if (n == 1)
            return 1;
    return n*factorial(n-1);
    }
    @PostMapping("/books")
    Book newBook(@RequestBody Book newBook) {
        return bookRepository.save(newBook);
    }
    @PostMapping("/test")
    void test (@RequestBody User user){
        userRepsitory.save(user);
    }
}